# 3D Renderer

This is a software 3D renderer for GNU/Linux systems written in pure ANSI C. It reads a [Wavefront OBJ file](https://en.wikipedia.org/wiki/Wavefront_.obj_file) and outputs an image in [PPM format](https://en.wikipedia.org/wiki/Netpbm_format#PPM_example), which you can view in your favourite image viewer.

Everything is currently written from scratch; image utilities, model file parser, 2D/3D vector utilities, etc. 

What's currently implemented:

- 2D A to B line draw
- Basic wireframe render ignoring perspective
- Triangle fill
- Basic flat shading render, again ignoring perspective. Uses a Z buffer for hidden surface determination
- UV mapping

There is much more functionality left to be added but I intend to implement them all from scratch too. This includes:

- Perspective projection
- Camera movement
- Shaders
- Normal mapping
- Shadow mapping
- Ambient occlusion

## Usage

Clone the repository:
```git clone https://gitlab.com/raan/c-3d-renderer.git```

cd into the repository:
```cd c-3d-renderer```

Compile the program:
```make```

Run the program:
```./main```

The program will output at `./image.ppm`
