#ifndef DRAW_H
#define DRAW_H

#include <math.h>
#include <limits.h>
#include "image.h"
#include "vec2i.h"
#include "vec2f.h"
#include "vec3f.h"
#include "model.h"

#define FALSE  0
#define TRUE   !FALSE

void draw_line(Vec2i* a, Vec2i* b, Image* image, Pixel* colour);
void render_wireframe(Model* model, Image* image, Pixel* colour);
void draw_triangle(Vec3f* points, float* zbuffer, Image* image, Pixel* colour);
void draw_tri_textured(Vec3f* points, float* zbuffer, Vec3f* tex_verts, Image* texture, float lightness, Image* image);
void draw_flat_shading(Model* model, Image* image);
void draw_textured(Model* model, Image* texture, Image* image);

#endif