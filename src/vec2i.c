#include "vec2i.h"

Vec2i* vec2i_create(int x, int y) {
	Vec2i* v = (Vec2i*)malloc(sizeof(Vec2i));
	v->x = x;
	v->y = y;
	return v;
}

/* gets the result of v0 + v1 and stores it in v0 */
void vec2i_add(Vec2i* v0, Vec2i* v1) {
	v0->x += v1->x;
	v0->y += v1->y;
}

/* gets the result of v0 - v1 and stores it in v0 */
void vec2i_subtract(Vec2i* v0, Vec2i* v1) {
	v0->x -= v1->x;
	v0->y -= v1->y;
}

/* multiplies given vector by scalar value c */
void vec2i_scalar_multiply(Vec2i* v, int c) {
	v->x *= c;
	v->y *= c;
}

/* multiplies vector by float scalar value f, rounds down */
void vec2i_scalar_multiply_f(Vec2i* v, float f) {
	v->x = floor((float)v->x * f);
	v->y = floor((float)v->y * f);
}

/* copies the contents of v1 into v0 */
void vec2i_cpy(Vec2i* v0, Vec2i* v1) {
	v0->x = v1->x;
	v0->y = v1->y;
}