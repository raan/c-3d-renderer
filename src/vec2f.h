#ifndef VEC_2F_H
#define VEC_2F_H

#include <stdlib.h>
#include "vec2i.h"

typedef struct Vec2f {
	float x;
	float y;
} Vec2f;

Vec2f* vec2f_create(float x, float y);
void vec2f_add(Vec2f* v0, Vec2f* v1);
void vec2f_subtract(Vec2f* v0, Vec2f* v1);

#endif