#ifndef IMAGE_H
#define IMAGE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Pixel {
	unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char a;
} Pixel;

typedef struct Image {
	int width;
	int height;
	Pixel* pixels;
} Image;

typedef struct TGA_Header {
	char id_length;
	char colour_map_type;
	char data_type_code;
	short int colour_map_origin;
	short int colour_map_length;
	char colour_map_depth;
	short int x_origin;
	short int y_origin;
	short int width;
	short int height;
	char bits_per_pixel;
	char image_descriptor;
} TGA_Header;

Image* image_create(int width, int height);
void image_destroy(Image* image);
void image_set_pixel(Image* image, int x, int y, Pixel* p);
void image_flip_vertically(Image* image);
void image_write_PPM(Image* image, const char* filename);
Image* image_load_TGA(const char* filename);
void image_write_TGA(Image* image, const char* filename);
void merge_bytes(Pixel* image_pixel, unsigned char* read_pixel, int n_bytes);

#endif