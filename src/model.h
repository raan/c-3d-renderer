#ifndef MODEL_H
#define MODEL_H

#include <stdio.h>
#include <stdlib.h>
#include "vec3f.h"

typedef struct Model {
	Vec3f* verts;
	Vec3f* texverts;
	int** faces;
	int n_verts;
	int n_texverts;
	int n_faces;
} Model;

Model* model_load(const char* filename);
void model_destroy(Model* model);

#endif