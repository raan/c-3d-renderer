#include "vec2f.h"

Vec2f* vec2f_create(float x, float y) {
	Vec2f* v = (Vec2f*)malloc(sizeof(Vec2f));
	v->x = x;
	v->y = y;
	return v;
}

/* gets the result of v0 + v1 and stores it in v0 */
void vec2f_add(Vec2f* v0, Vec2f* v1) {
	v0->x += v1->x;
	v0->y += v1->y;
}

/* gets the result of v0 - v1 and stores it in v0 */
void vec2f_subtract(Vec2f* v0, Vec2f* v1) {
	v0->x -= v1->x;
	v0->y -= v1->y;
}
