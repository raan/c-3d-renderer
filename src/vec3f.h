#ifndef VEC_3F_H
#define VEC_3F_H

#include <stdlib.h>
#include <math.h>

typedef struct Vec3f {
	float x;
	float y;
	float z;
} Vec3f;

Vec3f* vec3f_create(float x, float y, float z);
void vec3f_add(Vec3f* v0, Vec3f* v1);
void vec3f_subtract(Vec3f* v0, Vec3f* v1);
void vec3f_scalar_multiply(Vec3f* v, float c);
void vec3f_scalar_divide(Vec3f* v, float c);
float vec3f_magnitude(Vec3f* v);
void vec3f_normalise(Vec3f* v);
float vec3f_dot_product(Vec3f* u, Vec3f* v);
Vec3f vec3f_cross_product(Vec3f* u, Vec3f* v);
void vec3f_cpy(Vec3f* v0, Vec3f* v1);

#endif