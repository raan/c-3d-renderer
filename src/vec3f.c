#include "vec3f.h"

Vec3f* vec3f_create(float x, float y, float z) {
	Vec3f* vec = (Vec3f*)malloc(sizeof(Vec3f));
	vec->x = x;
	vec->y = y;
	vec->z = z;
	return vec;
}

/* gets the result of v0 + v1 and stores it in v0 */
void vec3f_add(Vec3f* v0, Vec3f* v1) {
	v0->x += v1->x;
	v0->y += v1->y;
	v0->z += v1->z;
}

/* gets the result of v0 - v1 and stores it in v0 */
void vec3f_subtract(Vec3f* v0, Vec3f* v1) {
	v0->x -= v1->x;
	v0->y -= v1->y;
	v0->z -= v1->z;
}

/* multiply vector v by scalar value c */
void vec3f_scalar_multiply(Vec3f* v, float c) {
	v->x *= c;
	v->y *= c;
	v->z *= c;
}

/* divide vector v by scalar value c */
void vec3f_scalar_divide(Vec3f* v, float c) {
	v->x /= c;
	v->y /= c;
	v->z /= c;
}

/* returns the magnitude of v */
float vec3f_magnitude(Vec3f* v) {
	return sqrt(v->x*v->x + v->y*v->y + v->z*v->z);
}

/* normalise a given vector (turn into unit vector in same direction) */
void vec3f_normalise(Vec3f* v) {
	vec3f_scalar_multiply(v, 1 / vec3f_magnitude(v));
}

/* returns the dot product of u and v */
float vec3f_dot_product(Vec3f* u, Vec3f* v) {
	return u->x*v->x + u->y*v->y + u->z*v->z;
}

/* returns the cross product of u and v */
Vec3f vec3f_cross_product(Vec3f* u, Vec3f* v) {
	Vec3f result;
	result.x = u->y*v->z - u->z*v->y;
	result.y = u->z*v->x - u->x*v->z;
	result.z = u->x*v->y - u->y*v->x;
	return result;
}

/* copies the contents of v1 into v0 */
void vec3f_cpy(Vec3f* v0, Vec3f* v1) {
	v0->x = v1->x;
	v0->y = v1->y;
	v0->z = v1->z;
}