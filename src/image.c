#include "image.h"

/* creates a new image struct for use */
Image* image_create(int width, int height) {
	int i;
	Image* img = (Image*)malloc(sizeof(Image));
	Pixel* pix = (Pixel*)calloc(width * height, sizeof(Pixel));
	for(i = 0; i < width * height; i++) {
		pix[i].r = 0;
		pix[i].g = 0;
		pix[i].b = 0;
		pix[i].a = 0;
	}

	img->width = width;
	img->height = height;
	img->pixels = pix;

	return img;
}

/* free an image */
void image_destroy(Image* image) {
	free(image->pixels);
	free(image);
}

/* set the pixel at coord (x, y) to the colour stored in p */
void image_set_pixel(Image* image, int x, int y, Pixel* p) {
	int width = image->width;

	image->pixels[width*y+x].r = p->r;
	image->pixels[width*y+x].g = p->g;
	image->pixels[width*y+x].b = p->b;
	image->pixels[width*y+x].a = p->a;
}

/* flips an image about the x axis */
void image_flip_vertically(Image* image) {
	int i;
	int width = image->width;
	int height = image->height;
	int half = height >> 1;
	int bytes_per_line = width * sizeof(Pixel);
	unsigned long l1, l2;

	/* temp storage for 1 row of pixels */
	Pixel* line = (Pixel*)calloc(width, sizeof(Pixel));

	/* swap line(i) with line(last - i) until hit halfway point */
	for(i = 0; i < half; i++) {
		l1 = i * width;
		l2 = (height - i - 1) * width;
		memmove(line,               &image->pixels[l1], bytes_per_line);
		memmove(&image->pixels[l1], &image->pixels[l2], bytes_per_line);
		memmove(&image->pixels[l2], line,               bytes_per_line);
	}

	/* cleanup */
	free(line);
}

/* outputs image to file in PPM format */
void image_write_PPM(Image* image, const char* filename) {
	int i, j;
	int r, g, b;
	int width = image->width;
	FILE* fp = fopen(filename, "w+");

	/* headers */
	fprintf(fp, "P3\n%d %d\n255\n", image->width, image->height);

	/* pixel data */
	for(i = 0; i < image->height; i++) {
		for(j = 0; j < image->width; j++) {
			r = image->pixels[i * width + j].r;
			g = image->pixels[i * width + j].g;
			b = image->pixels[i * width + j].b;

			fprintf(fp, "%d %d %d ", r, g, b);
		}
		fprintf(fp, "\n");
	}
	 fclose(fp);
}

/* loads a TGA file into the image pointed to */
/* currently, header information is discarded (apart from width/height) */
Image* image_load_TGA(const char* filename) {
	int i, n = 0;
	int header_rep_val;
	int bytes_per_pixel, bytes_to_skip;
	unsigned char pix_buf[5];
	FILE* fp;
	Image* image;
	Pixel* pixels;
	TGA_Header header;

	/* read TGA header */
	fp = fopen(filename, "rb");
	header.id_length = fgetc(fp);
	header.colour_map_type = fgetc(fp);
	header.data_type_code = fgetc(fp);
	fread(&header.colour_map_origin,2,1,fp);
	fread(&header.colour_map_length,2,1,fp);
	header.colour_map_depth = fgetc(fp);
	fread(&header.x_origin,2,1,fp);
	fread(&header.y_origin,2,1,fp);
	fread(&header.width,2,1,fp);
	fread(&header.height,2,1,fp);
	header.bits_per_pixel = fgetc(fp);
	header.image_descriptor = fgetc(fp);

	/* initialise image struct */
	image = (Image*)malloc(sizeof(Image));
	image->width = header.width;
	image->height = header.height;
	pixels = (Pixel*)calloc(header.width*header.height, sizeof(Pixel));
	for(i = 0; i < header.width*header.height; i++) {
		pixels[i].r = 0;
		pixels[i].g = 0;
		pixels[i].b = 0;
		pixels[i].a = 0;
	}

	/* skip past any colour map definitions since we won't be handling these */
	bytes_to_skip = header.id_length;
	bytes_to_skip += header.colour_map_type * header.colour_map_length;
	fseek(fp, bytes_to_skip, SEEK_CUR);

	bytes_per_pixel = header.bits_per_pixel / 8;
	while(n < header.width*header.height) {
		if(header.data_type_code == 2) {
			/* uncompressed true-colour TGA */
			/* can just read raw bytes and fill array */
			fread(pix_buf, 1, bytes_per_pixel, fp);
			merge_bytes(&pixels[n], pix_buf, bytes_per_pixel);
		}
		else if(header.data_type_code == 10) {
			/* run-length encoded true-colour */
			/* 1 pixel is defined by 1 byte header + 2,3 or 4 bytes of data */
			/* uses run-length encoding to compress consecutive colours */

			/* get the header */
			fread(pix_buf, 1, 1, fp);
			/* get last 7 bits of the header */
			header_rep_val = pix_buf[0] & 0x7f;
			
			if(pix_buf[0] & 0x80) { /* first bit is 1 */
				/* run-length packet */
				/* defines one colour value which should be repeated n times
				   where n is the repition val defined in packet header */
				/* read the colour, then insert it into pixel array
				   header_rep_val times */
				fread(pix_buf, 1, bytes_per_pixel, fp);
				for(i = 0; i < header_rep_val + 1; i++) {
					merge_bytes(&pixels[n], pix_buf, bytes_per_pixel);
					n++;
				}
			}
			else { /* first bit is 0 */
				/* raw packet */
				/* the next n x-byte entries will define the next n pixels
				   n = headeer_rep_val
				   x = bit depth of image defined in image header */
				for(i = 0; i < header_rep_val + 1; i++) {
					fread(pix_buf, 1, bytes_per_pixel, fp);
					merge_bytes(&pixels[n], pix_buf, bytes_per_pixel);
					n++;
				}
			}
		}
	}

	fclose(fp);
	image->pixels = pixels;
	return image;
}

/* outputs image to a file in TGA format */
/* will use true-colour with no compression */
void image_write_TGA(Image* image, const char* filename) {
	int i;
	FILE* fp;

	fp = fopen(filename, "wb");

	fputc(0x00, fp); /* id length*/
	fputc(0x00, fp); /* colour map type */
	fputc(0x02, fp); /* image type, 2 =uncompressed RGB */
	fputc(0x00, fp); fputc(0x00, fp); /* colour map offset */
	fputc(0x00, fp); fputc(0x00, fp); /* colour map length */
	fputc(0x00, fp); /* colour map bit-depth */
	fputc(0x00, fp); fputc(0x00, fp); /* x origin */
	fputc(0x00, fp); fputc(0x00, fp); /* y origin */
	fwrite(&image->width, 2, 1, fp); /* image width */
	fwrite(&image->height, 2, 1,fp); /* image height */
	fputc(0x18, fp); /* bits per pixel */
	fputc(0x00, fp); /* descriptor */

	/* image pixel data */
	for(i = 0; i < image->height*image->width; i++) {
		fwrite(&image->pixels[i].b, 1, 1, fp);
		fwrite(&image->pixels[i].g, 1, 1, fp);
		fwrite(&image->pixels[i].r, 1, 1, fp);
	}
	fclose(fp);
}

/* fills in the next pixel in the pixel array pointed to with the read pixel */
/* needs to know n_bytes since pixels in tga files can be 2, 3 or 4 bytes long */
void merge_bytes(Pixel* image_pixel, unsigned char* read_pixel, int n_bytes) {
	if(n_bytes == 3) {
		image_pixel->b = read_pixel[0];
		image_pixel->g = read_pixel[1];
		image_pixel->r = read_pixel[2];
		image_pixel->a = 255;
	}
}