#include "draw.h"

static Vec3f world_to_screen(Vec3f* v, int max_width, int max_height);
static float min_f(float x, float y);
static float max_f(float x, float y);
static Vec3f barycentric(Vec3f* a, Vec3f* b, Vec3f* c, Vec3f* p);
static void vec2i_swap(Vec2i* v0, Vec2i* v1);
static Vec3f tri_surface_normal(Vec3f* a, Vec3f* b, Vec3f* c);
static Pixel get_colour_from_texture(Vec3f* tex_verts, Vec3f bary_P, Image* texture);

/* bit-hack to swap two values */
#define SWAP(a, b) ((&(a) == &(b)) || (((a) -= (b)), ((b) += (a)), ((a) = (b) - (a))))

/* bithack for min & max int */
#define MIN(x, y) ((y) ^ (((x) ^ (y)) & -((x) < (y))))
#define MAX(x, y) ((x) ^ (((x) ^ (y)) & -((x) < (y))))

/* draws a line from (x0, y0) to (x1, y1) */
void draw_line(Vec2i* a, Vec2i* b, Image* image, Pixel* colour) {
	int x_out, y_out;
	float t;
	int steep = FALSE;
	/* extract coords from Vec2i structs */
	int x0 = a->x;
	int y0 = a->y;
	int x1 = b->x;
	int y1 = b->y;

	/* if the line height > width we transpose to make it shallow
	this fixes holes appearing in the line due to height being > width */
	if(abs(x0 - x1) < abs(y0 - y1)) {
		SWAP(x0, y0);
		SWAP(x1, y1);
		steep = TRUE;
	}
	/* make it left-to-right
	this ensures symmetry such that line(a,b) draws the same as line(b,a) */
	if(x0 > x1) {
		SWAP(x0, x1);
		SWAP(y0, y1);
	}

	/* for the length of the line, figure out y and set the pixel */
	for(x_out = x0; x_out <= x1; x_out++) {
		if(x0 == x1)
			t = 0;
		else
			t = (x_out-x0) / (float)(x1-x0);
		y_out = y0*(1.0 - t) + y1*t;
		if(steep) {
			/* if we transposed earlier, transpose it back */
			image_set_pixel(image, y_out, x_out, colour);
		}
		else {
			image_set_pixel(image, x_out, y_out, colour);
		}
	}
}

/* renders a wireframe of a .obj model & writes it to image */
void render_wireframe(Model* model, Image* image, Pixel* colour) {
	int i, j;
	int curr_face[3];
	Vec2i a, b; /* temp storage, used in rendering point a to b */
	Vec3f* v0 = (Vec3f*)malloc(sizeof(Vec3f));
	Vec3f* v1 = (Vec3f*)malloc(sizeof(Vec3f));

	int width = image->width;
	int height = image->height;

	/* for each face in the model */
	for(i = 0; i < model->n_faces; i++) {
		/* copy current face to do work on */
		memcpy(curr_face, model->faces[i], sizeof(int) * 3);

		/* make lines of pointA->pointB, pointB->pointC, pointC->pointA for this triangle */
		for(j = 0; j < 3; j++) {
			vec3f_cpy(v0, &model->verts[ curr_face[j] - 1 ]);
			vec3f_cpy(v1, &model->verts[ curr_face[(j+1) % 3] - 1 ]);

			/* scale the coords to our image size */
			a.x = (v0->x + 1.0) * (width - 1.0) / 2.0;
			a.y = (v0->y + 1.0) * (height - 1.0) / 2.0;
			b.x = (v1->x + 1.0) * (width - 1.0) / 2.0;
			b.y = (v1->y + 1.0) * (height - 1.0) / 2.0;

			draw_line(&a, &b, image, colour);
		}
	}

	/* cleanup temp vectors */
	free(v0);
	free(v1);
}

/* draw a triangle from 3 given points */
void draw_triangle(Vec3f* points, float* zbuffer, Image* image, Pixel* colour) {
	int i;
	Vec2f bound_min, bound_max, clamp;
	Vec3f P = {0.0, 0.0, 0.0};
	Vec3f bary_P = {0.0, 0.0, 0.0};

	/* get the bounding box for tri */
	bound_min.x = INT_MAX;
	bound_min.y = INT_MAX;
	bound_max.x = INT_MIN;
	bound_max.y = INT_MIN;
	clamp.x = image->width - 1;
	clamp.y = image->height - 1;
	for(i = 0; i < 3; i++) {
		bound_min.x = max_f(0, min_f(bound_min.x, points[i].x));
		bound_max.x = min_f(clamp.x, max_f(bound_max.x, points[i].x));

		bound_min.y = max_f(0, min_f(bound_min.y, points[i].y));
		bound_max.y = min_f(clamp.y, max_f(bound_max.y, points[i].y));
	}

	/* for each pixel in bounding box, check if that point lies on the tri & draw if it does */
	for(P.x = bound_min.x; P.x <= bound_max.x; P.x++) {
		for(P.y = bound_min.y; P.y <= bound_max.y; P.y++) {
			/* get the barycentric coords of P */
			bary_P = barycentric(&points[0], &points[1], &points[2], &P);
			/* if 0 <= bary_P < 1 then P is in the triangle, if not we can ignore this point */
			/* comparing with 0 produces artifacts, probably due to fp inaccuracy.
			   hack solution: comparing against small -ve vals instead */
			if(bary_P.x >= -1e-4 && bary_P.y >= -1e-4 && bary_P.z >= -1e-4) {
				P.z = 0;
				P.z += points[0].z * bary_P.x;
				P.z += points[1].z * bary_P.y;
				P.z += points[2].z * bary_P.z;

				/* check if we've already rendered something infront of P */
				if(zbuffer[(int)(P.x + P.y * image->width)] < P.z) {
					/* update z buffer */
					zbuffer[(int)(P.x + P.y * image->width)] = P.z;
					/* write pixel */
					image_set_pixel(image, P.x, P.y, colour);
				}
			}
		}
	}
}

/* draw a triangle from 3 given points & colour appropriately using texture */
/* a lot of code re use here, try refactor later */
void draw_tri_textured(Vec3f* points, float* zbuffer, Vec3f* tex_verts, Image* texture, float lightness, Image* image) {
	int i;
	Pixel colour;
	Vec2f bound_min, bound_max, clamp;
	Vec3f P = {0.0, 0.0, 0.0};
	Vec3f bary_P = {0.0, 0.0, 0.0};

	/* get the bounding box for tri */
	bound_min.x = INT_MAX;
	bound_min.y = INT_MAX;
	bound_max.x = INT_MIN;
	bound_max.y = INT_MIN;
	clamp.x = image->width - 1;
	clamp.y = image->height - 1;
	for(i = 0; i < 3; i++) {
		bound_min.x = max_f(0, min_f(bound_min.x, points[i].x));
		bound_max.x = min_f(clamp.x, max_f(bound_max.x, points[i].x));

		bound_min.y = max_f(0, min_f(bound_min.y, points[i].y));
		bound_max.y = min_f(clamp.y, max_f(bound_max.y, points[i].y));
	}

	/* for each pixel in bounding box, check if that point lies on the tri & draw if it does */
	for(P.x = bound_min.x; P.x <= bound_max.x; P.x++) {
		for(P.y = bound_min.y; P.y <= bound_max.y; P.y++) {
			/* get the barycentric coords of P */
			bary_P = barycentric(&points[0], &points[1], &points[2], &P);
			/* if 0 <= bary_P < 1 then P is in the triangle, if not we can ignore this point */
			/* comparing with 0 produces artifacts, probably due to fp inaccuracy.
			   hack solution: comparing against small -ve vals instead */
			if(bary_P.x >= -1e-4 && bary_P.y >= -1e-4 && bary_P.z >= -1e-4) {
				/* get colour of this pixel from texture */
				colour = get_colour_from_texture(tex_verts, bary_P, texture);
				colour.r *= lightness;
				colour.g *= lightness;
				colour.b *= lightness;

				/* grab the z value of P to check against z buffer */
				P.z = 0;
				P.z += points[0].z * bary_P.x;
				P.z += points[1].z * bary_P.y;
				P.z += points[2].z * bary_P.z;

				/* check if we've already rendered something infront of P */
				if(zbuffer[(int)(P.x + P.y * image->width)] < P.z) {
					/* update z buffer */
					zbuffer[(int)(P.x + P.y * image->width)] = P.z;
					/* write pixel */
					image_set_pixel(image, P.x, P.y, &colour);
				}
			}
		}
	}
}


/* draw all the faces in a model struct */
/* 1 flat colour, basic flat shading & z-buffer for hidden face determination */
void draw_flat_shading(Model* model, Image* image) {
	int i, j;
	int curr_face[3];
	Vec3f tri_coords[3]; /* 3 points of triangle in 3d space */
	Vec3f orig_coords[3];

	Pixel colour;
	Vec3f normal;
	float light_intensity;
	Vec3f light_vector = { 0, 0, -1 };

	int width = image->width;
	int height = image->height;

	/* initialise z-buffer */
	float* zbuffer = calloc(width*height, sizeof(float));
	for(i = 0; i < width*height; i++) {
		zbuffer[i] = INT_MIN;
	}

	for(i = 0; i < model->n_faces; i++) {
		memcpy(curr_face, model->faces[i], sizeof(int) * 3);

		for(j = 0; j < 3; j++) {
			orig_coords[j] = model->verts[curr_face[j] - 1];
			tri_coords[j] = world_to_screen(&model->verts[curr_face[j] - 1], width, height);
		}

		/* get triangle's surface normal & normalise */
		normal = tri_surface_normal(&orig_coords[2], &orig_coords[1], &orig_coords[0]);
		vec3f_normalise(&normal);

		/* light intensity = trinorm (dot) lightdirection */
		light_intensity = vec3f_dot_product(&normal, &light_vector);

		/* colours are just shades of white for now */
		colour.r = light_intensity*255;
		colour.g = light_intensity*255;
		colour.b = light_intensity*255;

		draw_triangle(tri_coords, zbuffer, image, &colour);
	}

	free(zbuffer);
}

/* render a given model with texturing */
/* UV mapped texture, basic shading and z-buffer */
void draw_textured(Model* model, Image* texture, Image* image) {
	int i, j;
	int curr_face[3];
	int curr_tex_verts[3];
	Vec3f tri_coords[3]; /* 3 points of triangle in 3d space */
	Vec3f orig_coords[3];
	Vec3f texture_verts[3];

	Vec3f normal;
	float light_intensity;
	Vec3f light_vector = { 0, 0, -1 };

	int width = image->width;
	int height = image->height;

	/* initialise z-buffer */
	float* zbuffer = calloc(width*height, sizeof(float));
	for(i = 0; i < width*height; i++) {
		zbuffer[i] = INT_MIN;
	}

	for(i = 0; i < model->n_faces; i++) {
		memcpy(curr_face, model->faces[i], sizeof(int) * 3); /* load in face index */
		memcpy(curr_tex_verts, &(model->faces[i][3]), sizeof(int) * 3); /* load in texture vertex index */

		/* get tri vertices & texture vertices from index */
		for(j = 0; j < 3; j++) {
			orig_coords[j] = model->verts[curr_face[j] - 1];
			tri_coords[j] = world_to_screen(&model->verts[curr_face[j] - 1], width, height);
			texture_verts[j] = model->texverts[curr_tex_verts[j] - 1];
		}

		/* get triangle's surface normal & normalise */
		normal = tri_surface_normal(&orig_coords[2], &orig_coords[1], &orig_coords[0]);
		vec3f_normalise(&normal);

		/* light intensity = trinorm (dot) lightdirection */
		light_intensity = vec3f_dot_product(&normal, &light_vector);

		draw_tri_textured(tri_coords, zbuffer, texture_verts, texture, light_intensity, image);
	}

	free(zbuffer);
}


/* returns the minimum of x and y */
static float min_f(float x, float y) {
	return x < y ? x : y;
}

/* returns the maximum of x and y */
static float max_f(float x, float y) {
	return x < y ? y : x;
}

/* convert world coord system to screen coord system */
static Vec3f world_to_screen(Vec3f* world, int max_width, int max_height) {
	Vec3f screen;

	screen.x = (int)((world->x + 1.0) * max_width / 2.0);
	screen.y = (int)((world->y + 1.0) * max_height / 2.0);
	screen.z = world->z;

	return screen;
}

/* returns the barycentric coordinates of a point P with respect to the triangle defined by points[] */
/* for how this is calculated refer to section 1.4 of:
   https://users.csc.calpoly.edu/~zwood/teaching/csc471/2017F/barycentric.pdf */
static Vec3f barycentric(Vec3f* a, Vec3f* b, Vec3f* c, Vec3f* p) {
	Vec3f result;
	Vec3f n, nA, nB, nC;
	float n_mag, divisor;

	n = tri_surface_normal(a,b,c);
	n_mag = vec3f_magnitude(&n);
	divisor = 1 / (n_mag * n_mag);
	nA = tri_surface_normal(b,c,p);
	nB = tri_surface_normal(c,a,p);
	nC = tri_surface_normal(a,b,p);

	result.x = vec3f_dot_product(&n, &nA) * divisor;
	result.y = vec3f_dot_product(&n, &nB) * divisor;
	result.z = vec3f_dot_product(&n, &nC) * divisor;

	return result;
}

/* swaps around two vectors */
/* could probably optimise this with pointers instead of copying */
static void vec2i_swap(Vec2i* v0, Vec2i* v1) {
	Vec2i* temp = (Vec2i*)malloc(sizeof(Vec2i));
	vec2i_cpy(temp, v0);
	vec2i_cpy(v0, v1);
	vec2i_cpy(v1, temp);
	free(temp);
}

/* returns the surface normal of the triangle defined by points p0,p1,p2 */
/* anti-clockwise direction, so n = (b - a) x (c - a) */
static Vec3f tri_surface_normal(Vec3f* a, Vec3f* b, Vec3f* c) {
	/* get vectors for 2 triangle sides */
	Vec3f v, w;
	/* V = b-a, W = c-a*/
	vec3f_cpy(&v, b);
	vec3f_subtract(&v, a);
	vec3f_cpy(&w, c);
	vec3f_subtract(&w, a);

	/* surface normal = V x W (where V and W are vectors of 2 triangle sides */
	return vec3f_cross_product(&v, &w);
}

/* for a triangle in the model, at point P, what colour should we use, 
based on the given texture? */
static Pixel get_colour_from_texture(Vec3f* tex_verts, Vec3f bary_P, Image* texture) {
	Vec3f tv0, tv1, tv2;
	Pixel colour;
	int tex_x, tex_y;
	int width = texture->width;
	int height = texture->height;

	/* make working copy of the texture verts */
	vec3f_cpy(&tv0, &tex_verts[0]);
	vec3f_cpy(&tv1, &tex_verts[1]);
	vec3f_cpy(&tv2, &tex_verts[2]);

	/* calculate what coord in the texture we want */
	/* p = bary1*tri1 + bary2*tri2 + bary3*tri3 
	   ie weighted average of tri coords, with weights being bary coords */
	vec3f_scalar_multiply(&tv0, bary_P.x);
	vec3f_scalar_multiply(&tv1, bary_P.y);
	vec3f_scalar_multiply(&tv2, bary_P.z);
	vec3f_add(&tv0, &tv1);
	vec3f_add(&tv0, &tv2);

	/* we have point in tex vert coord space, convert to actual tex coord system */
	tex_x = (int)(tv0.x*width + 0.5);
	tex_y = (int)(tv0.y*height + 0.5);

	/* grab the pixel from the texture */
	colour.r = texture->pixels[width*tex_y + tex_x].r;
	colour.g = texture->pixels[width*tex_y + tex_x].g;
	colour.b = texture->pixels[width*tex_y + tex_x].b;

	return colour;
}