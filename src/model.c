/* Operations for internal model struct */

#include "model.h"

/* load .obj file into model struct */
/* could do with a refactor, feels like a messy implementation */
Model* model_load(const char* filename) {
	float x, y, z; /* temp storage for vertex values */
	Vec3f* temp; /* temp storage for current vertex */
	int a, b, c, d, e, f, g, h, i; /* temp storage for face values*/
	char line[1000]; /* temp storage for curr line*/
	FILE* fp = fopen(filename, "r");
	Model* model = (Model*)malloc(sizeof(Model));
	/* vars to keep track */
	int verts_count = 0, curr_vert = 0;
	int faces_count = 0, curr_face = 0;
	int texverts_count = 0, curr_texvert = 0;

	if(ferror(fp)) {
		perror("Error opening model file");
	}

	/* first pass to get # of verts and faces */
	while(fgets(line, 1000, fp) != NULL) {
		if(line[0] == 'v' && line[1] == ' ') {
			verts_count++;
		}
		else if(line[0] == 'f' && line[1] == ' ') {
			faces_count++;
		}
		else if(line[0] == 'v' && line[1] == 't' && line[2] == ' ') {
			texverts_count++;
		}
	}
	fclose(fp);

	/* initialise verts and faces arrays */
	model->verts = (Vec3f*)calloc(verts_count, sizeof(Vec3f));
	model->texverts = (Vec3f*)calloc(texverts_count, sizeof(Vec3f));
	model->faces = (int**)calloc(faces_count, sizeof(int*));
	for(i = 0; i < faces_count; i++) {
		/* each faces element only stores 9 vars */
		/* 3 vertex indices, 3 texture vertex indices, 3 vertex normal indicies */
		model->faces[i] = (int*)calloc(9, sizeof(int));
	}
	model->n_verts = verts_count;
	model->n_texverts = texverts_count;
	model->n_faces = faces_count;

	/* second pass to fill model struct */
	fp = fopen(filename, "r");
	while(fgets(line, 1000, fp) != NULL) {
		if(sscanf(line, "v %f %f %f\n", &x, &y, &z)) {
			/* vertex index */
			temp = vec3f_create(x, y, z);
			model->verts[curr_vert] = *temp;
			curr_vert++;
			free(temp);
		}
		else if(sscanf(line, "vt %f %f %f\n", &x, &y, &z)) {
			/* vertex normal index */
			temp = vec3f_create(x, y, z);
			model->texverts[curr_texvert] = *temp;
			curr_texvert++;
			free(temp);
		}
		else if(sscanf(line, "f %d/%d/%d %d/%d/%d %d/%d/%d\n", &a, &d, &g, &b, &e, &h, &c, &f, &i)) {
			/* ^ lol */

			/* map face elements to faces array */
			/* array structure looks like: 
			   [0, 1, 2] = vertex index
			   [3, 4, 5] = vertex texture index
			   [6, 7, 8] = vertex normal index

			   ie:
			   "f v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3"
			   ==> [v1,v2,v3, vt1,vt2,vt3, vn1,vn2,vn3]
   			*/
			model->faces[curr_face][0] = a;
			model->faces[curr_face][1] = b;
			model->faces[curr_face][2] = c;

			model->faces[curr_face][3] = d;
			model->faces[curr_face][4] = e;
			model->faces[curr_face][5] = f;

			model->faces[curr_face][6] = g;
			model->faces[curr_face][7] = h;
			model->faces[curr_face][8] = i;
			curr_face++;
		}
	}

	fclose(fp);
	return model;
}

void model_destroy(Model* model) {
	int i;
	for(i = 0; i < model->n_faces; i++) {
		free(model->faces[i]);
	}
	free(model->verts);
	free(model->texverts);
	free(model->faces);
	free(model);
}