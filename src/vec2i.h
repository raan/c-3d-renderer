#ifndef VEC_2I_H
#define VEC_2I_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct Vec2i {
	int x;
	int y;
} Vec2i;

Vec2i* vec2i_create(int x, int y);
void vec2i_add(Vec2i* v1, Vec2i* v2);
void vec2i_subtract(Vec2i* v1, Vec2i* v2);
void vec2i_scalar_multiply(Vec2i* v, int c);
void vec2i_scalar_multiply_f(Vec2i* v, float f);
void vec2i_cpy(Vec2i* v0, Vec2i* v1);

#endif