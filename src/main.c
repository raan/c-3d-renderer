#include <stdio.h>
#include "vec2i.h"
#include "vec2f.h"
#include "vec3f.h"
#include "model.h"
#include "draw.h"
#include "image.h"

void test_wireframe() {
	/* wireframe test */
	Image* image;
	Pixel white = {255, 255, 255, 255};
	Model* m = model_load("obj/african_head.obj");

	printf("Rendering wireframe model...");
	image = image_create(500, 500);
	render_wireframe(m, image, &white);
	image_flip_vertically(image);
	image_write_PPM(image, "wireframe.ppm");
	printf(" ok\n");

	model_destroy(m);
	image_destroy(image);
}

void test_tri_fill() {
	/* tri fill test */
	Image* image = image_create(500, 500);
	float* zbuffer = (float*)calloc(500*500, sizeof(float));
	Pixel white = {255, 255, 255, 255};
	Pixel red =   {255, 0, 0, 225};
	Pixel green = {0, 255, 0, 255};
	Vec3f t0[3] = { {10, 70, 10},   {50, 160, 10},  {70, 80, 10} };
	Vec3f t1[3] = { {180, 50, 10},  {150, 1, 10},   {70, 180, 10} };
	Vec3f t2[3] = { {180, 150, 10}, {120, 160, 10}, {130, 180, 10} };

	printf("Rendering triangle fill test...");
	draw_triangle(t0, zbuffer, image, &red);
	draw_triangle(t1, zbuffer, image, &white);
	draw_triangle(t2, zbuffer, image, &green);
	image_flip_vertically(image);
	image_write_PPM(image, "tri_fill.ppm");
	printf(" ok\n");

	image_destroy(image);
	free(zbuffer);
}

void test_flat_shade() {
	/* flat shading render test */
	Image* image = image_create(500, 500);
	Model* m = model_load("obj/african_head.obj");

	printf("Rendering flat shaded model...");
	draw_flat_shading(m, image);
	image_flip_vertically(image);
	image_write_PPM(image, "flat_shading.ppm");
	printf(" ok\n");

	model_destroy(m);
	image_destroy(image);
}

void test_tga_load_save() {
	/* TGA load test */
	Image* image;

	printf("Running TGA load & save test...");
	image = image_load_TGA("obj/african_head_diffuse.tga");
	image_write_TGA(image, "tga_loadsave.tga");
	printf(" ok\n");

	image_destroy(image);
}

void test_texture_mapping() {
	/* texture mapping render test */
	Image* image_out = image_create(1500, 1500);
	Model* m = model_load("obj/african_head.obj");
	Image* tex = image_load_TGA("obj/african_head_diffuse.tga");

	printf("Rendering texture mapped model...");
	draw_textured(m, tex, image_out);
	image_flip_vertically(image_out);
	image_write_PPM(image_out, "texture_mapped.ppm");
	printf(" ok\n");

	model_destroy(m);
	image_destroy(tex);
	image_destroy(image_out);
}

int main(int argc, char* argv[])
{
	test_wireframe();
	test_tri_fill();
	test_flat_shade();
	test_tga_load_save();
	test_texture_mapping();
	return 0;
}