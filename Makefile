CFLAGS = -Wall -pedantic -ansi -g
OBJ = src/main.o src/image.o src/draw.o src/vec2i.o src/vec2f.o src/vec3f.o src/model.o
EXEC = main

$(EXEC): $(OBJ)
	gcc $(OBJ) -lm -o $(EXEC)

main.o: src/main.c
	gcc $(CFLAGS) -c src/main.c

image.o: src/image.c src/image.h
	gcc $(CFLAGS) -c src/image.c

draw.o: src/draw.c src/draw.h
	gcc $(CFLAGS) -c src/draw.c

vec2i.o: src/vec2i.c src/vec2i.h
	gcc $(CFLAGS) -c src/vec2i.o

vec2f.o: src/vec2f.c vec2f.h
	gcc $(CFLAGS) -c src/vec2f.o

vec3f.o: src/vec3f.c src/vec3f.h
	gcc $(CFLAGS) -c src/vec3f.o

model.o: src/model.c src/model.h
	gcc $(CFLAGS) -c src/model.o

clean:
	rm -f main $(OBJ) *.ppm *.tga